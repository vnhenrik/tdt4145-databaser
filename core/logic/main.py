from core.logic.CoffeeController import register_user, log_in


def main():
    print("Welcome to Coffee database.\n"
          "Log in or register a user?")

    valid_options = ['log in', 'register', 'exit']
    choice = input("type 'log in' or 'register: ")

    while choice not in valid_options:
        print("Invalid choice selected!")
        choice = input("type 'log in' or 'register: ")

    if choice == 'log in':
        log_in()
    elif choice == 'register':
        register_user()
    elif choice == 'exit':
        print("Bye!")


main()
