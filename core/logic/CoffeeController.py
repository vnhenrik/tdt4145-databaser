import datetime

from database.database import db_register_user, db_log_in, db_review_coffee, db_exit, \
    db_update_coffee_score, db_find_coffee_country, db_get_stats, \
    db_search_coffeenotes, db_display_best_coffee, coffee_not_exists_in_db


# Adds a new user to the User table.
def register_user():
    print("--------------------\nPlease register a user.")
    firstname = input("First name: ")
    lastname = input("Last name: ")
    email = input("email: ")
    password = input("Password: ")
    db_register_user(firstname, lastname, email, password)
    log_in()


# Logs a user in given the details are correct.
def log_in():
    print("--------------------\nPlease log in.")
    email = input("Please enter your email: ")
    password = input("Enter your password: ")
    logged_in, email, userid = db_log_in(email, password)
    if logged_in is False:
        return None
    else:
        main(logged_in, email, userid)


# Reviews a coffee. Collects input from the user and passes them to the database function.
def coffee_review():
    print("--------------------\nReview a coffee!")
    coffeename = input("Coffee name: ")
    while coffee_not_exists_in_db(coffeename):
        print("Oops! We have never heard of this coffee. Did you spell it correctly?"
              "\nTips! Use the 'best coffees' command to see available coffees sorted by score")
        coffeename = input("Coffee name: ")
    distillery = input("Coffee distillery: ")
    try:
        points = int(input("Points between 0-10: "))
    except ValueError:
        points = int(input("Please input a valid number between 0-10: "))

    date = datetime.datetime.now()
    note = input("Please write a short  review: ")
    return coffeename, distillery, points, date, note


# Finds the coffees based on input. Collects input from the user and passes them to the database function.
def find_coffee():
    print("--------------------\nFind your coffee!")
    processing = input("Enter a processing method: ")
    country = input("Country of origin: ")
    return processing, country

# Simply calls on the corresponding database function.
def get_stats():
    print("--------------------\nStats")
    db_get_stats()

def search_notes():
    print("--------------------\nSearch notes")
    valid_choices = ["coffeeReview","coffee"]
    term = input("What's the word you want to search for?: " )
    table = input("Write [coffeeReview] for search in coffee reviews, [coffee] for search in description of coffees: ")
    while table not in valid_choices:
        table = input(
            "Write [coffeeReview] for search in coffee reviews, [coffee] for search in description of coffees: ")

    term = '%'+term+'%'
    return term, table


def main(logged_in, email, userid):
    print(f"--------------------\nWelcome to Coffee Review {email}")
    while logged_in:
        choices = ["review", "find coffee", "exit", "stats", "search notes", "best coffees"]
        print("What would you like to do?\nChoices: review, find coffee, stats, exit, search notes, best coffees")
        choice = input("action >: ")

        while choice not in choices:
            print("Invalid option selected\nOptions: review, find coffee, exit, best coffees ")
            choice = input("action >: ")

        if choice == 'review':
            coffeename, distillery, points, date, note = coffee_review()
            db_review_coffee(userid, coffeename, distillery, points, date, note)
            db_update_coffee_score(coffeename)

        elif choice == "search notes":
            term, table = search_notes()
            db_search_coffeenotes(table, term)

        elif choice == 'best coffees':
            db_display_best_coffee()

        elif choice == 'find coffee':
            processing, country = find_coffee()
            db_find_coffee_country(processing, country)

        elif choice == 'stats':
            get_stats()

        elif choice == 'exit':
            db_exit()
            break