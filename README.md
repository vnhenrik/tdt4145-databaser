**Registering/Inlogging**
* Først kjører du main.py. Deretter skriver du "register". Her oppretter du en bruker.
* Deretter blir man automatisk redirigert til logg inn modulen.
* Logg inn med dine epost-adressen din.
* dd

**Brukerhistorie 1**
* Logg inn og velg 'review'.
* Nå fyller du bare inn detaljer og brukerhistorien er løst.

**Brukerhistorie 2**
* Logg inn og velg 'stats'.
* Alt printes automatisk og brukerhistorien er løst.

**Brukerhistorie 5**
* Logg inn og velg 'find coffee'.
* Sorter på foredlingsmetode og land
* Brukerhistorien er nå løst.