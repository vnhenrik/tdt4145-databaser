import sqlite3
from sqlite3 import Error


# Connects to the database 'Coffee.db'
def create_connection():
    try:
        con = sqlite3.connect("../../database/Coffee.db")
        cursor = con.cursor()
        print("Connected to database.")
        return con, cursor

    except Error as e:
        print("Could not connect to database!")
        print(e)


# Creates tables based on the textfile 'Coffe.txt'
def create_tables():
    con, cursor = create_connection()

    with open("../../database/Coffee.txt", encoding="utf-8") as file:
        script = file.read()
        cursor.executescript(script)
        print("Successfully created tables.")
        con.commit()
        con.close()


con, cursor = create_connection()


# Inserts a user into the User table.
def db_register_user(firstName, lastName, email, password):
    try:
        cursor.execute("""INSERT INTO user
                                  (firstName, lastName, email, password) 
                                   VALUES (?,?,?,?)""", (firstName, lastName,
                                                         email, password))
        con.commit()
        print(f"Successfully inserted user {firstName}  {lastName} into database!")

    except Error as e:
        print(e)


# Logs a user in given it exists in the User table and the credentials are correct.
def db_log_in(email, password):
    try:
        cursor.execute("""SELECT * FROM User'
                          ' WHERE (email = ? AND password = ?)""", (email, password))

        row = cursor.fetchone()
        if row is None:
            print("Invalid username or password.")
            logged_in = False
            return logged_in, None, None
        else:
            logged_in = True
            userid = get_userID(email)
            return logged_in, email, userid
    except Error as e:
        print(e)


# returns userID based on email.
def get_userID(email):
    try:
        cursor.execute("""SELECT userId FROM User'
                                 ' WHERE (email = ?)""", [email])
        row = cursor.fetchone()

        if row is None:
            print("Couldn't fetch userID!\n"
                  "Couldn't find email in User table")
        else:
            for rows in row:
                userid = rows
                return userid
    except Error as e:
        print(e)


# Inserts a review into the coffeeReview table.
def db_review_coffee(userid, coffeeName, distilleryName, points, date, note):
    try:
        cursor.execute("""INSERT INTO coffeeReview
                                      (userid, coffeeName, distilleryName, points, date, note) 
                                       VALUES 
                                      (?,?,?,?,?,?)""", (userid, coffeeName, distilleryName,
                                                         points, date, note))
        con.commit()
        print(f"Successfully inserted your review for {coffeeName} into the database!")

    except Error as e:
        print(e)


# search trough notes.
def db_search_coffeenotes(table, searchterm):
    if table == "coffeeReview":
        try:
            cursor.execute("""SELECT c.coffeeName, c.distilleryName
                                        FROM coffeeReview
                                        INNER JOIN coffee c on coffeeReview.coffeeName = c.coffeeName
                                        WHERE coffeeReview.note LIKE ?""", [searchterm])
            row = cursor.fetchall()
            if not row:
                print("No results for your query!")
            if row is None:
                print("No results for your query!")
            else:
                print("---------------------------")
                for results in row:
                    coffee = results[0]
                    distillery = results[1]
                    print(f"Coffee name: {coffee}\nDistillery: {distillery}\n ---------------------------")
        except Error as e:
            print(e)

    if table == "coffee":
        print("selected coffee")
        try:
            cursor.execute("""SELECT coffeeName, distilleryName FROM coffee
                                            WHERE coffee.description LIKE ?""", [searchterm])
            result = cursor.fetchall()
            if not result:
                print("No results for your query!")
                print("-------------------------------------")
            if result is None:
                print("No results for your query")
                print("-------------------------------------")
            else:
                print("---------------------------")
                for results in result:
                    coffee = results[0]
                    distillery = results[1]
                    print(f"Coffee name: {coffee}\nDistillery: {distillery}\n ---------------------------")
        except Error as e:
            print(e)


# Simply returns the score of the selected coffee from the coffee table. Help function
def db_get_coffee_score(coffeename):
    # get the score from the selected coffee
    try:
        cursor.execute("""SELECT sum(points) FROM coffeeReview'
                                 ' WHERE (coffeename = ?)""", [coffeename])
        row = cursor.fetchone()
        # returns a tuple, score is in row[0]
        score = row[0]
        if score is None:
            score = 0
        return score

    except Error as e:
        print(e)


# Counts the amount of times the input has been reviewed in coffeeReview. Used to divide score to get average.
# Help function
def db_count_coffee_reviews(coffeename):
    try:
        cursor.execute("""SELECT coffeeName,
                                count(*) as c FROM coffeeReview'
                                ' WHERE (coffeename = ?)""", [coffeename])
        row = cursor.fetchone()
        # returns a tuple, so we access it in a for-loop
        times = row[1]
        return times

    except Error as e:
        print(e)


# Function that updates the coffee inputs average score.
def db_update_coffee_score(coffeename):
    coffeescore = db_get_coffee_score(coffeename)
    count = db_count_coffee_reviews(coffeename)
    new_score = coffeescore / count

    try:
        cursor.execute("""UPDATE coffee
                    SET averageScore = ?
                    WHERE coffeeName = ?""", (new_score, coffeename))
        con.commit()
        print(f"Updated score for {coffeename}")
    except Error as e:
        print(e)


# exit function, closes cursor and connection.
def db_exit():
    cursor.close()
    con.close()
    print("bye!")


# Finds coffee based on processing method and country.
def db_find_coffee_country(processing, country):
    try:
        cursor.execute("""SELECT c.coffeeName, c.distilleryName
                            FROM coffeeBatch
                            INNER JOIN processing p on coffeeBatch.batchID = p.batchID
                            INNER JOIN Farm F on coffeeBatch.farmID = F.farmID
                            INNER JOIN coffee c on coffeeBatch.batchID = c.batchID
                            WHERE p.method = ? AND F.country = ?""", (processing, country))

        print("-------------------------------------")
        row = cursor.fetchall()

        if not row:
            print("No results for your query!")
            print("-------------------------------------")
        if row is None:
            print("No results for your query")
            print("-------------------------------------")
        else:
            for i in row:
                print(f"Coffee name: {i[0]} \nDistillery: {i[1]}")
                print("-------------------------------------")

    except Error as e:
        print(e)


# Returns a list of users that has reviewed the most unique coffees.
def db_get_stats():
    try:
        cursor.execute((""" SELECT (u.firstName  || ' '|| U.lastName) AS fullName,
                        COUNT(DISTINCT coffeeName) AS UniqueCoffee
                        FROM coffeeReview
                        INNER JOIN User u on coffeeReview.userID = u.userId
                        WHERE date(date) BETWEEN '2022-01-01 00:00:00' AND '2022-12-31 00:00:00'
                        GROUP BY fullName
                        ORDER BY COUNT(coffeeName) DESC"""))

        rows = cursor.fetchall()
        print("-------------------------------------")
        for i in rows:
            name = i[0]
            count = i[1]
            print(f"{name} has tasted a total of {count} unique coffees in 2022.")
            print("-------------------------------------")

    except Error as e:
        print(e)


# printer alle kaffer sortert etter ønsket averageScore (3)
def db_display_best_coffee():
    try:
        cursor.execute("""SELECT coffeeName, distilleryName, kiloPrice, averageScore
                            FROM coffee ORDER BY averageScore DESC""")
        row = cursor.fetchall()

        if row is None:
            print("Couldn't find any coffees")

        for coffee in row:
            name = coffee[0]
            distillery = coffee[1]
            kgprice = coffee[2]
            score = coffee[3]
            print(f"Coffee: {name}, distillery: {distillery}, Price(kg): {kgprice}, score: {score}")

    except Error as e:
        print(e)


def coffee_not_exists_in_db(name):
    try:
        cursor.execute("""SELECT coffeeName
           FROM coffee""")
        row = cursor.fetchall()

        validate_list = []
        for coffee in row:
            validate_list.append(coffee[0])
        if name in validate_list:
            return False
        return True

    except Error as e:
        print(e)
